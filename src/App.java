import com.devcamp.j50_javabasic.s10.NewDevcampApp;
public class App {
    public static void main(String[] args) throws Exception {
        // This is comment
        String appName = "Devcamp will help everyone to know coding";
        System.out.println("Hello, World" + appName.length()); // This is comment
        System.out.println("uppercase" + appName.toUpperCase()); // This is comment
        System.out.println("LOWERCASE: " + appName.toLowerCase() );
        NewDevcampApp.name("Bao Tran", 30);
        NewDevcampApp newApp = new NewDevcampApp();
        newApp.name("Tran The Bao");
    }
}

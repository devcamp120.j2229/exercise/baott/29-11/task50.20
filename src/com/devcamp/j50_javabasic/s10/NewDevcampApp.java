package com.devcamp.j50_javabasic.s10;

public class NewDevcampApp {
    public static void main(String[] args){
        System.out.println("I'm a new devcamp app");
        NewDevcampApp.name("BaoTran", 30);
        NewDevcampApp newApp = new NewDevcampApp();
        newApp.name("Bao Tran");
    }
    public void name(String name){
        System.out.println("My name is" + name);
    }
    public static void name(String name, int age){
        System.out.println("My name is" + name + "age is" + age);
    }
    
}
